----------------------------------------------------------------------------------
-- Company: INFN & University of Bologna
-- Developer: Nico Giangiacomi 
-- 
-- Create Date: 02/13/2018 04:43:37 PM
-- Design Name: clock_manager
-- Module Name: clock_manager - Behavioral
-- Project Name: ZC706_Protocol_Converter
-- Target Devices: ZC706 Xilinx Evaluation Board
-- Tool Versions: 1.0
-- Description: The purpose of this module is to provide clock sources to the board
--
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity clock_manager is
Port ( 
	-- RESET
	rst					: in std_logic;
	
	-- INPUT CLOCKS
	SYSCLK_P			: in std_logic;
	SYSCLK_N			: in std_logic;
	SMA_MGT_REFCLK_N	: in std_logic;
	SMA_MGT_REFCLK_P	: in std_logic;
	SI5326_OUT_C_N		: in std_logic;
	SI5326_OUT_C_P		: in std_logic;	
	recclk				: in std_logic;
	USER_CLOCK_P        : in std_logic;
	USER_CLOCK_N        : in std_logic;
	
	--  OUTPUT CLOCKS
	REC_CLOCK_C_N		: out std_logic;
	REC_CLOCK_C_P		: out std_logic;
	sysclk				: out std_logic;	--200 MHz clk
	sysclk_40			: out std_logic;	--40 MHz clk from sysclk
	clk40				: out std_logic;	--40 mHz clk from recclk
	clk80				: out std_logic;	--80 mHz clk from recclk
	clk120				: out std_logic;	--120 mHz clk from recclk
	clk160				: out std_logic;	--160 MHz clk from recclk
	clk240				: out std_logic;	--240 MHz clk from recclk
	sma_mgt_refclk		: out std_logic;	--240 MHz clk
	pll_mgt_refclk		: out std_logic;	--240 MHz clk
    USER_SMA_CLOCK_P	: out std_logic;
    USER_SMA_CLOCK_N    : out std_logic;
	
	rec_clk_pll_locked	: out std_logic
);
end clock_manager;

architecture Behavioral of clock_manager is
----------------------------------------------------------
--                                                      --
--                  Signal Declaration                  --
--                                                      --
----------------------------------------------------------
	signal sysclk_i                                 : std_logic;
	signal recovered_clk_out						: std_logic;
	signal sma_mgt_refclk_i, pll_mgt_refclk_i		: std_logic;
	signal recclk_i, clk_debug_i					: std_logic;
	signal clkfb									: std_logic;
	signal usr_clk, sma_clk                         : std_logic;

----------------------------------------------------------
--                                                      --
--                 Module Declaration                  --
--                                                      --
----------------------------------------------------------
	component clk_wiz_120 is
	Port (
		-- Clock out ports
		clk40_out		: out std_logic;
		clk120_out		: out std_logic;
		clk160_out		: out std_logic;
		clk240_out		: out std_logic;
		clk80_out		: out std_logic;
		-- Status and control signals
		reset			: in std_logic;
		locked			: out std_logic;
		-- Clock in ports
		clk_in1			: in std_logic
	); 
	end component clk_wiz_120;

	component clk_wiz_200
	Port (
	-- Clock out ports
		clk200_out          : out    std_logic;
		clk40_out          : out    std_logic;
	-- Status and control signals
		reset             : in     std_logic;
		locked            : out    std_logic;
		clk_in1_p         : in     std_logic;
		clk_in1_n         : in     std_logic
	);
	end component clk_wiz_200;

begin

	sysclk			<= sysclk_i;
	sma_mgt_refclk	<= sma_mgt_refclk_i;
	pll_mgt_refclk	<= pll_mgt_refclk_i;
		
	--SMA refclk for MGT
	sma_refclk_inst: ibufds_gte2
	port map (
		O							=> sma_mgt_refclk_i,
		ODIV2						=> open,
		CEB							=> '0',
		I							=> SMA_MGT_REFCLK_P,
		IB							=> SMA_MGT_REFCLK_N
	);
		
	--Si5324 PLL refclk for MGT
	pll_refclk_inst: ibufds_gte2
	port map (
		O							=> pll_mgt_refclk_i,
		ODIV2						=> open,
		CEB							=> '0',
		I							=> SI5326_OUT_C_P,
		IB							=> SI5326_OUT_C_N
	);


	--obuf for forwarding recovered clock
	ODDR_inst : ODDR
	generic map(
		DDR_CLK_EDGE => "OPPOSITE_EDGE", 
		INIT => '0',  
		SRTYPE => "SYNC") 
	port map (
		Q => recovered_clk_out,   
		C => recclk,    
		CE => '1',  
		D1 => '0',  
		D2 => '1',  
		R => '0',    
		S => '0'     
	);
	
	OBUFDS_inst : OBUFDS
	generic map (
		IOSTANDARD => "DEFAULT", 
		SLEW => "FAST")          
	port map (
		O => REC_CLOCK_C_P,     
		OB => REC_CLOCK_C_N,   
		I => recovered_clk_out     
	);
	
	
---------------------------------------------------------
--        USER_CLOCK --->  USER_SMA forwarding         --
---------------------------------------------------------

    IBUFDS_usrclk : IBUFDS
        generic map (
        DIFF_TERM => TRUE,
        IBUF_LOW_PWR => FALSE,
        IOSTANDARD => "DEFAULT")
    port map (
        O => usr_clk,
        I => USER_CLOCK_P,
        IB => USER_CLOCK_N
    );
    
    ODDR_smaclk : ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE",
        INIT => '0',
        SRTYPE => "SYNC")
    port map (
        Q => sma_clk,
        C => usr_clk,
        CE => '1',
        D1 => '0',
        D2 => '1',
        R => '0',
        S => '0'
    );


    OBUFDS_smaclk : OBUFDS
    generic map (
    IOSTANDARD => "DEFAULT",
    SLEW => "FAST")
    port map (
    O => USER_SMA_CLOCK_P,
    OB => USER_SMA_CLOCK_N,
    I => sma_clk
    );



----------------------------------------------------------
--                                                      --
--                 Module Implementation                --
--                                                      --
----------------------------------------------------------
	--clk manager for 120 MHz rec clk
	clock_wizard_recclk: clk_wiz_120
	Port Map(
		-- Clock out ports
		clk40_out		=> clk40,
		clk120_out		=> clk120,
		clk160_out		=> clk160,
		clk240_out		=> clk240,
		clk80_out		=> clk80,
		-- Status and control signals
		reset			=> rst,
		locked			=> rec_clk_pll_locked,
		-- Clock in ports
		clk_in1			=> recclk
	); 
	
	--clk manager for sysclk
	clock_wizard_sysclk : clk_wiz_200
	port map ( 
		-- Clock out ports  
		clk200_out		=> sysclk_i,
		clk40_out		=> sysclk_40,
		-- Status and control signals                
		reset			=> '0',
		locked			=> open,
		-- Clock in ports
		clk_in1_p		=> SYSCLK_P,
		clk_in1_n		=> SYSCLK_N
	);

end Behavioral;
