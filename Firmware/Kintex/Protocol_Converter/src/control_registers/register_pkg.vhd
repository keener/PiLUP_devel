library ieee;
use ieee.std_logic_1164.all;

package register_types is
	subtype reg is std_logic_vector(31 downto 0);
	type reg_matrix is array (natural range <>) of reg;

	-- CONTROL registers
	constant N_CTRL_REGS		  : integer	:= 256;
	constant reset_r              : natural := 0;
	constant reset_range          : natural := 0;
	constant SFP_TX_DISABLE_r     : natural := 1;
	constant SFP_TX_DISABLE_range : natural := 8;
	constant TTC_en_r             : natural := 1;
	constant TTC_en_range         : natural := 16;
	constant n_hits_r             : natural := 1;
	type n_hits_range is range 31 downto 24;

	-- STATUS registers
	constant N_STATUS_REGS			  : integer	:= 256;
	constant rec_clk_pll_locked_r     : natural := 0;
	constant rec_clk_pll_locked_range : natural := 0;
	constant cpll_locked_r            : natural := 0;
	constant cpll_locked_range        : natural := 1;
	constant qpll_locked_r            : natural := 0;
	constant qpll_locked_range        : natural := 2;

	constant REGS_AXI_ADDR_WIDTH : integer := 31;

end package;
