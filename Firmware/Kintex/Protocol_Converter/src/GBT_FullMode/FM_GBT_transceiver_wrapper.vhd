----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/05/2018 01:35:25 PM
-- Design Name: 
-- Module Name: FM_GBT_transceiver_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.FMTransceiverPackage.all;


entity FM_GBT_transceiver_wrapper is
Generic (
	NUM_LINKS		: integer := 1
);
Port ( 
	-- clocks
	SYSCLK_IN				: in std_logic;
	sma_mgt_refclk			: in std_logic;
	pll_mgt_refclk			: in std_logic;
	rxusrclk				: out std_logic;
	txusrclk				: out std_logic;
	
	-- reset ports
	cpllreset_in			: in std_logic;
	SOFT_RESET_TX_IN		: in std_logic;
	SOFT_RESET_RX_IN		: in std_logic;
	
	-- SFP ports
	gtrxp_in				: in   std_logic_vector(NUM_LINKS-1 downto 0);
	gtrxn_in				: in   std_logic_vector(NUM_LINKS-1 downto 0);
	gttxn_out				: out  std_logic_vector(NUM_LINKS-1 downto 0);
	gttxp_out				: out  std_logic_vector(NUM_LINKS-1 downto 0);

	-- data
	rxdata_out				: out slv40_array(NUM_LINKS-1 downto 0);
	txdata_in				: in slv32_array(NUM_LINKS-1 downto 0);
	txcharisk_in			: in slv4_array(NUM_LINKS-1 downto 0);
	
	--control ports
	rxslide_in				: in std_logic_vector(NUM_LINKS-1 downto 0);
	TX_FSM_RESET_DONE_OUT	: out std_logic_vector(NUM_LINKS-1 downto 0);
	RX_FSM_RESET_DONE_OUT	: out std_logic_vector(NUM_LINKS-1 downto 0);
	cplllock_out			: out std_logic_vector(NUM_LINKS-1 downto 0);
	qplllock_out			: out std_logic;
	rxresetdone_out			: out std_logic_vector(NUM_LINKS-1 downto 0);
	txresetdone_out			: out std_logic_vector(NUM_LINKS-1 downto 0)
);
end FM_GBT_transceiver_wrapper;

architecture Behavioral of FM_GBT_transceiver_wrapper is
----------------------------------------------------------
--                                                      --
--                 Signal Declaration                   --
--                                                      --
----------------------------------------------------------
	signal qplloutclk_i, qplloutrefclk_i, qplllock_i		: std_logic;
	signal qpllrefclklost_i, qpllreset_i					: std_logic;
	signal txusrclk_i, rxusrclk_i							: std_logic;
	signal txoutclk_i, rxoutclk_i							: std_logic_vector(NUM_LINKS-1 downto 0);
	
----------------------------------------------------------
--                                                      --
--                 Module Declaration                   --
--                                                      --
----------------------------------------------------------
    component GBT_FM_transceiver_gtx 
    port
    (
        SYSCLK_IN                               : in   std_logic;
        SOFT_RESET_TX_IN                        : in   std_logic;
        SOFT_RESET_RX_IN                        : in   std_logic;
        DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
        GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
        GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
        GT0_DATA_VALID_IN                       : in   std_logic;
        
        --_________________________________________________________________________
        --GT0  (X0Y0)
        --____________________________CHANNEL PORTS________________________________
        --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out                   : out  std_logic;
        gt0_cplllock_out                        : out  std_logic;
        gt0_cplllockdetclk_in                   : in   std_logic;
        gt0_cpllreset_in                        : in   std_logic;
        -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtnorthrefclk0_in                   : in   std_logic;
        gt0_gtnorthrefclk1_in                   : in   std_logic;
        gt0_gtrefclk0_in                        : in   std_logic;
        gt0_gtrefclk1_in                        : in   std_logic;
        gt0_gtsouthrefclk0_in                   : in   std_logic;
        gt0_gtsouthrefclk1_in                   : in   std_logic;
        ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                          : in   std_logic_vector(8 downto 0);
        gt0_drpclk_in                           : in   std_logic;
        gt0_drpdi_in                            : in   std_logic_vector(15 downto 0);
        gt0_drpdo_out                           : out  std_logic_vector(15 downto 0);
        gt0_drpen_in                            : in   std_logic;
        gt0_drprdy_out                          : out  std_logic;
        gt0_drpwe_in                            : in   std_logic;
        ------------------------------- Clocking Ports -----------------------------
        gt0_txsysclksel_in                      : in   std_logic_vector(1 downto 0);
        --------------------------- Digital Monitor Ports --------------------------
        gt0_dmonitorout_out                     : out  std_logic_vector(7 downto 0);
        --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in                     : in   std_logic;
        gt0_rxuserrdy_in                        : in   std_logic;
        -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out                : out  std_logic;
        gt0_eyescantrigger_in                   : in   std_logic;
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                         : in   std_logic;
        gt0_rxusrclk2_in                        : in   std_logic;
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                          : out  std_logic_vector(39 downto 0);
        --------------------------- Receive Ports - RX AFE -------------------------
        gt0_gtxrxp_in                           : in   std_logic;
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gtxrxn_in                           : in   std_logic;
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxdfelpmreset_in                    : in   std_logic;
        gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
        gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclk_out                        : out  std_logic;
        gt0_rxoutclkfabric_out                  : out  std_logic;
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                        : in   std_logic;
        gt0_rxpmareset_in                       : in   std_logic;
        ---------------------- Receive Ports - RX gearbox ports --------------------
        gt0_rxslide_in                          : in   std_logic;
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out                     : out  std_logic;
        --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                        : in   std_logic;
        gt0_txuserrdy_in                        : in   std_logic;
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt0_txusrclk_in                         : in   std_logic;
        gt0_txusrclk2_in                        : in   std_logic;
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                           : in   std_logic_vector(31 downto 0);
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gtxtxn_out                          : out  std_logic;
        gt0_gtxtxp_out                          : out  std_logic;
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclk_out                        : out  std_logic;
        gt0_txoutclkfabric_out                  : out  std_logic;
        gt0_txoutclkpcs_out                     : out  std_logic;
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        gt0_txcharisk_in                        : in   std_logic_vector(3 downto 0);
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out                     : out  std_logic;
        
        
        --____________________________COMMON PORTS________________________________
        GT0_QPLLLOCK_IN : in std_logic;
        GT0_QPLLREFCLKLOST_IN  : in std_logic;
        GT0_QPLLRESET_OUT  : out std_logic;
        GT0_QPLLOUTCLK_IN  : in std_logic;
        GT0_QPLLOUTREFCLK_IN : in std_logic
    
    );
    end component;
    
    
    
begin

----------------------------------------------------------
--                                                      --
--                 Module Implementation                --
--                                                      --
----------------------------------------------------------
	qplllock_out			<= qplllock_i;
	rxusrclk				<= rxusrclk_i;
	txusrclk				<= txusrclk_i;
	
	txoutclk_bufg0_i : BUFG
	port map
	(
		I				=>      txoutclk_i(0),
		O				=>      txusrclk_i
	);
	
	rxoutclk_bufg1_i : BUFG
	port map
	(
		I				=>      rxoutclk_i(0),
		O				=>      rxusrclk_i
	);
  
	
	
	
	
    gtxe2_common_i : GTXE2_COMMON
    generic map
    (
            -- Simulation attributes
            SIM_RESET_SPEEDUP    => "TRUE",
            SIM_QPLLREFCLK_SEL   => "001",
            SIM_VERSION          => "4.0",


       ------------------COMMON BLOCK Attributes---------------
        BIAS_CFG                                =>     (x"0000040000001000"),
        COMMON_CFG                              =>     (x"00000000"),
        QPLL_CFG                                =>     (x"0680181"),
        QPLL_CLKOUT_CFG                         =>     ("0000"),
        QPLL_COARSE_FREQ_OVRD                   =>     ("010000"),
        QPLL_COARSE_FREQ_OVRD_EN                =>     ('0'),
        QPLL_CP                                 =>     ("0000011111"),
        QPLL_CP_MONITOR_EN                      =>     ('0'),
        QPLL_DMONITOR_SEL                       =>     ('0'),
        QPLL_FBDIV                              =>     ("0010000000"),
        QPLL_FBDIV_MONITOR_EN                   =>     ('0'),
        QPLL_FBDIV_RATIO                        =>     ('1'),
        QPLL_INIT_CFG                           =>     (x"000006"),
        QPLL_LOCK_CFG                           =>     (x"21E8"),
        QPLL_LPF                                =>     ("1111"),
        QPLL_REFCLK_DIV                         =>     (1)

        
    )
    port map
    (
        ------------- Common Block  - Dynamic Reconfiguration Port (DRP) -----------
        DRPADDR                         =>      (others => '0'),
        DRPCLK                          =>      '0',
        DRPDI                           =>      (others => '0'),
        DRPDO                           =>      open,
        DRPEN                           =>      '0',
        DRPRDY                          =>      open,
        DRPWE                           =>      '0',
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GTGREFCLK                       =>      '0',
        GTNORTHREFCLK0                  =>      '0',
        GTNORTHREFCLK1                  =>      '0',
        GTREFCLK0                       =>      '0',
        GTREFCLK1                       =>      '0',
        GTSOUTHREFCLK0                  =>      '0',
        GTSOUTHREFCLK1                  =>      pll_mgt_refclk,
        ------------------------- Common Block -  QPLL Ports -----------------------
        QPLLDMONITOR                    =>      open,
        ----------------------- Common Block - Clocking Ports ----------------------
        QPLLOUTCLK                      =>      qplloutclk_i,
        QPLLOUTREFCLK                   =>      qplloutrefclk_i,
        REFCLKOUTMONITOR                =>      open,
        ------------------------- Common Block - QPLL Ports ------------------------
        QPLLFBCLKLOST                   =>      open,
        QPLLLOCK                        =>      qplllock_i,
        QPLLLOCKDETCLK                  =>      sysclk_in,
        QPLLLOCKEN                      =>      '1',
        QPLLOUTRESET                    =>      '0',
        QPLLPD                          =>      '0',
        QPLLREFCLKLOST                  =>      qpllrefclklost_i,
        QPLLREFCLKSEL                   =>      "110",
        QPLLRESET                       =>      qpllreset_i,
        QPLLRSVD1                       =>      "0000000000000000",
        QPLLRSVD2                       =>      "11111",
        --------------------------------- QPLL Ports -------------------------------
        BGBYPASSB                       =>      '1',
        BGMONITORENB                    =>      '1',
        BGPDB                           =>      '1',
        BGRCALOVRD                      =>      "11111",
        PMARSVD                         =>      "00000000",
        RCALENB                         =>      '1'

    );

	transceiver_inst: for i in 0 to NUM_LINKS-1 generate
		GBT_FM_transceiver_inst : GBT_FM_transceiver_gtx
		port map
		(
			SYSCLK_IN                       => SYSCLK_IN,
			SOFT_RESET_TX_IN                => SOFT_RESET_TX_IN,
			SOFT_RESET_RX_IN                => SOFT_RESET_RX_IN,
			DONT_RESET_ON_DATA_ERROR_IN     => '1',
			GT0_TX_FSM_RESET_DONE_OUT 		=> TX_FSM_RESET_DONE_OUT(i),
			GT0_RX_FSM_RESET_DONE_OUT 		=> RX_FSM_RESET_DONE_OUT(i),
			GT0_DATA_VALID_IN 				=> '1',
			
			--_________________________________________________________________________
			--GT0  (X0Y0)
			--____________________________CHANNEL PORTS________________________________
			--------------------------------- CPLL Ports -------------------------------
			gt0_cpllfbclklost_out           => open,
			gt0_cplllock_out                => cplllock_out(i),
			gt0_cplllockdetclk_in           => sysclk_in,
			gt0_cpllreset_in                => cpllreset_in,
			-------------------------- Channel - Clocking Ports ------------------------
			gt0_gtnorthrefclk0_in           => '0',
			gt0_gtnorthrefclk1_in           => '0',
			gt0_gtrefclk0_in                => '0',
			gt0_gtrefclk1_in                => sma_mgt_refclk,
			gt0_gtsouthrefclk0_in           => '0',
			gt0_gtsouthrefclk1_in           => '0',
			---------------------------- Channel - DRP Ports  --------------------------
			gt0_drpaddr_in                  => (others => '0'),
			gt0_drpclk_in                   => sysclk_in,
			gt0_drpdi_in                    => (others => '0'),
			gt0_drpdo_out                   => open,
			gt0_drpen_in                    => '0',
			gt0_drprdy_out                  => open,
			gt0_drpwe_in                    => '0',
			------------------------------- Clocking Ports -----------------------------
			gt0_txsysclksel_in              => "11",
			--------------------------- Digital Monitor Ports --------------------------
			gt0_dmonitorout_out             => open,
			--------------------- RX Initialization and Reset Ports --------------------
			gt0_eyescanreset_in             =>  '0',
			gt0_rxuserrdy_in                =>  '0',
			-------------------------- RX Margin Analysis Ports ------------------------
			gt0_eyescandataerror_out        =>  open,
			gt0_eyescantrigger_in           =>  '0',
			------------------ Receive Ports - FPGA RX Interface Ports -----------------
			gt0_rxusrclk_in                 =>  rxusrclk_i,
			gt0_rxusrclk2_in                =>  rxusrclk_i,
			------------------ Receive Ports - FPGA RX interface Ports -----------------
			gt0_rxdata_out                  =>  rxdata_out(i),
			--------------------------- Receive Ports - RX AFE -------------------------
			gt0_gtxrxp_in                   =>  gtrxp_in(i),
			------------------------ Receive Ports - RX AFE Ports ----------------------
			gt0_gtxrxn_in                   =>  gtrxn_in(i),
			--------------------- Receive Ports - RX Equalizer Ports -------------------
			gt0_rxdfelpmreset_in            =>  '0',
			gt0_rxmonitorout_out            =>  open,
			gt0_rxmonitorsel_in             =>  "00",
			--------------- Receive Ports - RX Fabric Output Control Ports -------------
			gt0_rxoutclk_out				=>  rxoutclk_i(i),
			gt0_rxoutclkfabric_out          =>  open,
			------------- Receive Ports - RX Initialization and Reset Ports ------------
			gt0_gtrxreset_in                =>  '0',
			gt0_rxpmareset_in               =>  '0',
			---------------------- Receive Ports - RX gearbox ports --------------------
			gt0_rxslide_in                  =>   rxslide_in(i),
			-------------- Receive Ports -RX Initialization and Reset Ports ------------
			gt0_rxresetdone_out             =>   rxresetdone_out(i),
			--------------------- TX Initialization and Reset Ports --------------------
			gt0_gttxreset_in                =>   '0',
			gt0_txuserrdy_in                =>   '0',
			------------------ Transmit Ports - FPGA TX Interface Ports ----------------
			gt0_txusrclk_in                 =>   txusrclk_i,
			gt0_txusrclk2_in                =>   txusrclk_i,
			------------------ Transmit Ports - TX Data Path interface -----------------
			gt0_txdata_in                   =>   txdata_in(i),
			---------------- Transmit Ports - TX Driver and OOB signaling --------------
			gt0_gtxtxn_out                  =>   gttxn_out(i),
			gt0_gtxtxp_out                  =>   gttxp_out(i),
			----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
			gt0_txoutclk_out                =>   txoutclk_i(i),
			gt0_txoutclkfabric_out          =>   open,
			gt0_txoutclkpcs_out             =>   open,
			--------------------- Transmit Ports - TX Gearbox Ports --------------------
			gt0_txcharisk_in                =>   txcharisk_in(i),
			------------- Transmit Ports - TX Initialization and Reset Ports -----------
			gt0_txresetdone_out             =>   txresetdone_out(i),
			
			
			--____________________________COMMON PORTS________________________________
			GT0_QPLLLOCK_IN 				=> qplllock_i, 
			GT0_QPLLREFCLKLOST_IN 			=> qpllrefclklost_i, 
			GT0_QPLLRESET_OUT 				=> qpllreset_i, 
			GT0_QPLLOUTCLK_IN  				=> qplloutclk_i,
			GT0_QPLLOUTREFCLK_IN 			=> qplloutrefclk_i 
		
		);
	end generate;
end Behavioral;
