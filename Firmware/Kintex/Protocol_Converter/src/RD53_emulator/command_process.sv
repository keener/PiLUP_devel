// Take the input data from chip output that is not trigger
// data, and process it. Some of the commands that directed
// toward front end chips are simply flagged on their respective
// output flags. Others, like reading and writing to the
// configuration registers, change the state of the system, e.g.
// a write to address 0 will change the value in the register
// at zero, which a read of address 0 will reflect.

// Pulse and cal are currently not implemented beyond countdown.

// testing: command_process.do

module command_process(
    input rst,
    input clk80,
    input data_in_valid,
    input [7:0] data_in,
    input [3:0] chip_id,
    output reg ECR, BCR, pulse, cal, 
               wrreg, rdreg, sync,
    output reg data_out_valid,
    output reg [15:0] data_out,
    output reg [8:0] register_address,
    output reg register_address_valid,
    output reg CAL_edge, CAL_aux,
    output reg [15:0] config_reg_out [511:0]
);

// states
localparam NEUTRAL_S = 4'd0, PULSE_S = 4'd1, CAL_S = 4'd2, 
           WRREG_S = 4'd3, NOOP_S = 4'd4, RDREG_S = 4'd5, 
           SYNC_S = 4'd6, ECR_S = 4'd7, BCR_S = 4'd8;
// command encodings
// note, RdReg and WrReg are the same up until bit 32
// therefore, their encoding is the same here;
// the code defaults to RD_WR_REG until the 32nd bit
localparam ECR_C = 8'h5A, BCR_C = 8'h59, G_PULSE_C = 8'h5C,
           CAL_C = 8'h63,  NOOP_C = 8'h69, RDREG_C = 8'h65,
           WRREG_C = 8'h66, sync_pattern = 16'b1000_0001_0111_1110;

reg [3:0] state;
reg [3:0] state_store;  // store the current state while receiving a sync

reg [4:0] dataword;

reg [15:0] config_reg [511:0];
reg [8:0] config_reg_adx;
reg [15:0] config_reg_write;

reg [4:0] words_left;
reg [4:0] words_store;  // see above
reg [4:0] globalpulsecnt;
reg [6:0] caledgecnt;
reg [4:0] caledgedelay;
reg [5:0] calauxdelay;
reg calpulse, calauxactive;

wire ECR_comb, BCR_comb, pulse_comb, cal_comb, 
     noop_comb, wrreg_comb, rdreg_comb, sync_comb;

assign config_reg_out = config_reg;

// combinational logic for command detect
assign ECR_comb = (data_in == ECR_C);
assign BCR_comb = (data_in == BCR_C);
assign pulse_comb = (data_in == G_PULSE_C);
assign cal_comb = (data_in == CAL_C);
assign noop_comb = (data_in == NOOP_C);
assign rdreg_comb = (data_in == RDREG_C);
assign wrreg_comb = (data_in == WRREG_C);
assign sync_comb = (data_in == sync_pattern[15:8]);

// decode encoded data stream
always @(*) begin: decode_dataword
   if      (data_in == 8'h6A) dataword = 5'd0;
   else if (data_in == 8'h6C) dataword = 5'd1;
   else if (data_in == 8'h71) dataword = 5'd2;
   else if (data_in == 8'h72) dataword = 5'd3;
   else if (data_in == 8'h74) dataword = 5'd4;
   else if (data_in == 8'h8B) dataword = 5'd5;
   else if (data_in == 8'h8D) dataword = 5'd6;
   else if (data_in == 8'h8E) dataword = 5'd7;
   else if (data_in == 8'h93) dataword = 5'd8;
   else if (data_in == 8'h95) dataword = 5'd9;
   else if (data_in == 8'h96) dataword = 5'd10;
   else if (data_in == 8'h99) dataword = 5'd11;
   else if (data_in == 8'h9A) dataword = 5'd12;
   else if (data_in == 8'h9C) dataword = 5'd13;
   else if (data_in == 8'hA3) dataword = 5'd14;
   else if (data_in == 8'hA5) dataword = 5'd15;
   else if (data_in == 8'hA6) dataword = 5'd16;
   else if (data_in == 8'hA9) dataword = 5'd17;
   else if (data_in == 8'hAA) dataword = 5'd18;
   else if (data_in == 8'hAC) dataword = 5'd19;
   else if (data_in == 8'hB1) dataword = 5'd20;
   else if (data_in == 8'hB2) dataword = 5'd21;
   else if (data_in == 8'hB4) dataword = 5'd22;
   else if (data_in == 8'hC3) dataword = 5'd23;
   else if (data_in == 8'hC5) dataword = 5'd24;
   else if (data_in == 8'hC6) dataword = 5'd25;
   else if (data_in == 8'hC9) dataword = 5'd26;
   else if (data_in == 8'hCA) dataword = 5'd27;
   else if (data_in == 8'hCC) dataword = 5'd28;
   else if (data_in == 8'hD1) dataword = 5'd29;
   else if (data_in == 8'hD2) dataword = 5'd30;
   else if (data_in == 8'hD4) dataword = 5'd31;
   else dataword = 5'dx;
end: decode_dataword
    
always @(posedge clk80 or posedge rst) begin
    
    if (rst) begin
        ECR <= 1'b0;
        BCR <= 1'b0;
        wrreg <= 1'b0;
        rdreg <= 1'b0;
        sync <= 1'b0;
        rdreg <= 1'b0;
        cal <= 1'b0;
        
        data_out <= 16'b0;

        state <= NEUTRAL_S;
        state_store <= NEUTRAL_S;
        words_store <= 5'd0;
        
        config_reg_adx <= 9'b0;
        config_reg_write <= 16'b0;
    end
        
    else begin  // not rst
    
        // we want to set these signals low in the neutral state 
        // even if there is no new incoming data
        if (state == NEUTRAL_S) begin
            ECR <= 1'b0;
            BCR <= 1'b0;
            pulse <= 1'b0;
            cal <= 1'b0;
            wrreg <= 1'b0;
            rdreg <= 1'b0;
            sync <= 1'b0;
            
            data_out_valid <= 1'b0;
            register_address_valid <= 1'b0;
        end
        
        if (data_in_valid) begin
            
            if (sync_comb) begin
                sync <= 1'b1;
                state <= SYNC_S;
                state_store <= state;
                words_left <= 5'd0;
                words_store <= words_left;
            end
            
            else begin
                case (state)
                
                    // Upon receipt of the first half of a SYNC command.
                    SYNC_S: begin
                        if (words_left == 5'd0) begin
                            if (data_in == sync_pattern[7:0]) begin
                                sync <= 1'b0;
                                state <= state_store;
                                words_left <= words_store;
                            end
                            else begin
                                $display("ASSERTION FAILED at %d, state = %d, sync mismatch", $time, state);
                            end
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end   
           
                    NEUTRAL_S: begin
                        if (ECR_comb) begin               
                            state <= ECR_S;
                            words_left <= 5'd0;
                        end
                        else if (BCR_comb) begin
                            state <= BCR_S;
                            words_left <= 5'd0;
                        end
                        else if (pulse_comb) begin
                            state <= PULSE_S;
                            words_left <= 5'd2;
                        end
                        else if (cal_comb) begin
                            state <= CAL_S;
                            words_left <= 5'd4;
                        end
                        else if (noop_comb) begin
                            state <= NOOP_S;
                            words_left <= 5'd0;
                        end
                        else if (rdreg_comb) begin
                            state <= RDREG_S;
                            words_left <= 5'd4;
                        end
                        else if (wrreg_comb) begin 
                            state <= WRREG_S;
                            words_left <= 5'd6;
                        end
                    end
                    
                    // Upon receipt of a ECR command.
                    ECR_S: begin
                        if (ECR_comb) begin
                            ECR <= 1'b1;
                        end
                        else begin
                            $display("Repeated command not found in state %d, time %d", state, $time);
                        end
                        state <= NEUTRAL_S;
                    end
                    
                    // Upon receipt of a BCR command.
                    BCR_S: begin
                        if (BCR_comb) begin
                            BCR <= 1'b1;
                        end
                        else begin
                            $display("Repeated command not found in state %d, time %d", state, $time);
                        end
                        state <= NEUTRAL_S;
                    end
                    
                    // Upon receipt of a GLOBAL PULSE command. Not fully implemented; loads globalpulsecnt
                    // register based on second word, and returns control to neutral. globalpulsecount flags
                    // reg pulse and counts down. 
                    PULSE_S: begin
                        if (words_left == 5'd2) begin
                            if (pulse_comb) begin
                                pulse <= 1'b1;
                                words_left <= 5'b1;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end
                        else if (words_left == 5'd1) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin  // if ID does not match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd0) begin
                            globalpulsecnt <= dataword;
                            state <= NEUTRAL_S;
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end 
                    
                    // Upon receipt of a CAL command. Since there is no branch crossing clock
                    // in the emulator, branch crossings are approximated using the recovered clock.
                    CAL_S: begin
                        if (words_left == 5'd4) begin
                            if (cal_comb) begin
                                cal <= 1'b1;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end
                        else if (words_left == 5'd3) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin 
                            // if ID doesn't match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            //if (dataword[0]) begin
                            //    $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            //end
                            calpulse <= dataword[0];
                        end
                        else if (words_left == 5'd2) begin
                            //calpulse <= dataword[4];
                            caledgedelay <= 2 * dataword[4:2];  // counts down at 80Mhz, therefore doubled
                            caledgecnt	<= 32 * dataword[1:0];
                        end
                        else if (words_left == 5'd1) begin
                            caledgecnt <= caledgecnt + 2 * dataword[4:1];
                            calauxactive <= dataword[0];
                        end
                        else if (words_left == 5'd0) begin
                            calauxdelay <= 2 * dataword;
                            state <= NEUTRAL_S;
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    
                    NOOP_S: begin
                        if (words_left == 5'd0) begin
                            if (!noop_comb) begin
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                            state <= NEUTRAL_S;
                        end
                    end
                    
                    // Data structure is config_reg. Specialty registers are not yet implemented.
                    // Instead, it is used to store data. Given that there are no pixels to configure, 
                    // we don't anticipate this being an issue.
                    RDREG_S: begin
                        if (words_left == 5'd4) begin
                            if (rdreg_comb) begin
                                rdreg <= 1'b1;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end
                        else if (words_left == 5'd3) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin  // if ID doesn't match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd2) begin
                            config_reg_adx[8:4] <= dataword;
                        end
                        else if (words_left == 5'd1) begin
                            config_reg_adx[3:0] <= dataword[4:1];
                            
                            if (dataword[0]) begin
                                $display($time, "RDREG no trailing 0 after address");
                            end
                        end
                        else if (words_left == 5'd0) begin
                            data_out <= config_reg[config_reg_adx];
                            data_out_valid <= 1'b1;
                            
                            register_address <= config_reg_adx;
                            register_address_valid <= 1'b1;
                            
                            if (dataword != 5'b0) begin
                                $display("RDREG last word != 5'b0 at %d0", $time);
                            end
                            $display("New read at %d, adx %h", $time, config_reg_adx); 
                            rdreg <= 1'b0;
                            state <= NEUTRAL_S;
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    
                    WRREG_S: begin
                        if (words_left == 5'd6) begin
                            if (wrreg_comb) begin
                                wrreg <= 1'b1;
                                words_left <= 5'd5;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end                
                        else if (words_left == 5'd5) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin  // if ID doesn't match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                wrreg <= 1'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd4) begin
                            config_reg_adx[8:4] <= dataword;
                        end
                        else if (words_left == 5'd3) begin
                            {config_reg_adx[3:0], config_reg_write[15]} <= dataword;
                        end
                        else if (words_left == 5'd2) begin
                            config_reg_write[14:10] <= dataword;
                        end
                        else if (words_left == 5'd1) begin
                            config_reg_write[9:5] <= dataword;
                        end
                        else if (words_left == 5'd0) begin
                            config_reg[config_reg_adx] <= {config_reg_write[15:5], dataword};
                            state <= NEUTRAL_S;
                            wrreg <= 1'b0;
                            $display("New write at %d, adx %h: %h", $time, config_reg_adx,
                                {config_reg_write[15:5], dataword}); 
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    default: begin
                        ECR <= 1'b0;
                        BCR <= 1'b0;
                        pulse <= 1'b0;
                        cal <= 1'b0;
                        wrreg <= 1'b0;
                        rdreg <= 1'b0;
                        sync <= 1'b0;
                        
                        data_out_valid <= 1'b0;
                        register_address_valid <= 1'b0;
                        
                        state <= NEUTRAL_S;
                        words_left <= 5'd0;      
                        
                         $display("UNKNOWN COMMAND at %d", $time);      
                    end
                endcase
            end  // else (!sync_comb)
        end  // data_in_valid
    end // not rst
    
//    // make sure to set the valid signals low on every cycle they're not set high
//    else if (!data_in_valid) begin
//        data_out_valid <= 1'b0;
//        register_address_valid <= 1'b0;
//    end
    
    // count down the global pulse, ignores data_in_valid
    if (rst) begin
        globalpulsecnt <= 5'b0;
        pulse <= 1'b0;
    end
    else if (globalpulsecnt != 5'b0) begin
        globalpulsecnt <= globalpulsecnt - 1;
        pulse <= 1'b1;
    end
    else begin
        pulse <= 1'b0;
    end
    
    // count down words left
    if (rst) words_left <= 5'b0;
    else if (data_in_valid && words_left) words_left = words_left - 1;

    // count down cal, approximated to match BX clock
    if (rst) begin
        caledgecnt <= 7'd0;
        calauxdelay <= 6'd0;
        caledgedelay <= 5'd0;
        calpulse <= 1'b1;
        calauxactive <= 1'b0;
        CAL_edge <= 1'b0;
        CAL_aux <= 1'b0;
    end
    else begin
        if (|caledgedelay) begin
            caledgedelay <= caledgedelay - 5'b1;
        end
        else if ((|caledgecnt) | (~calpulse)) begin 
        // if pulse is still active, or step
            CAL_edge <= 1'b1;
            if (|caledgecnt) caledgecnt <= caledgecnt - 7'b1;
        end
        
        if (|calauxdelay) begin
            calauxdelay <= calauxdelay - 6'b1;
        end
        else if (calauxactive) begin
            CAL_aux <= 1'b1;
        end
    end
end
 
endmodule




