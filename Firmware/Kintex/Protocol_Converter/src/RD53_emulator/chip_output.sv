// Takes data in from TTC_top after formatting to 16 bit words.
// This data is latched to the local clock domain, so it is sent through
// a FIFO to be put in the domain of the recovered clock. The recovered
// clock itself is at 40 MHz on input, so it is doubled using a PLL to allow
// the 16 bit data words to be split into 8 bit segments (the size of the
// triggers / commands). 
// The data is then passed into the trigger FSM, which detects if the word is a
// trigger. If it is, the trigger pattern is shifted out. If it is not, the
// word is passed to the command FSM, which processes it. If the command results
// in generated data, the data is then passed to command_out.

// testing: no testing availble

module chip_output(
   input rst, clk160, clk40, clk80,
   input word_valid,
   input [15:0] data_in,
   input [3:0] chip_id,
   input [7:0] n_hits_in,
   output [63:0] aurora_data_out[4],
   output [1:0] aurora_head_out,
   output trig_out,
   output fifo_full,
   output TT_full,
   output TT_empty,
   output ECR_o, BCR_o, pulse_o, cal_o, wrreg_o, rdreg_o, sync_o
);

// states
localparam IDLE_S = 1'b0, TRIGGER_S = 1'b1;

reg [3:0] trig_sr_i, trig_sr;
reg cmd_valid_i, trig_valid;

wire wr_cmd, wr_adx;
wire [63:0] processed_hit;
wire processed_hit_val;
wire [15:0] processed_cmd;
wire [8:0] processed_adx;
wire [7:0] fifo_data;
wire fifo_data_valid, trig_done, cmd_full, adx_full, rd_word, cmd_valid;
wire pll_locked;
wire next_hit, CAL_aux, CAL_edge;
wire BCR, ECR;

reg [25:0] autoreg_dataA[4], autoreg_dataB[4];
reg [8:0] autoreg_addA[4], autoreg_addB[4];

reg state = IDLE_S;
reg [4:0] trig_tag = 5'b00000;
reg trig_valid_i = 1'b0; 
reg [15:0] BCID_cnt = 16'h0000;
reg [15:0] config_reg [511:0];


wire fifo_full_i;
assign fifo_full = cmd_full || adx_full || fifo_full_i;
assign rd_word = (!state || trig_done) && fifo_data_valid && pll_locked;
assign cmd_valid = cmd_valid_i && fifo_data_valid && pll_locked;
assign pll_locked = 1'b1;
//assign trig_valid_i = !cmd_valid_i && fifo_data_valid && pll_locked;


//Cross that uncertain clock domain
//Input width is 16, output width is 8
//Depth 16
word_fifo_gen word_fifo(
   .rst(rst),
   .wr_clk(clk160),
   .rd_clk(clk80),
   .din(data_in),
   .wr_en(word_valid),
   .rd_en(rd_word & pll_locked),
   .dout(fifo_data),
   .full(fifo_full_i),
   .empty(),
   .valid(fifo_data_valid)
);

//Decode FSM; decode and decide if trigger or command
always @ (posedge clk80 or posedge rst) begin 
	if (rst) begin
		trig_sr_i = 4'b0000;
		cmd_valid_i = 1'b0;
		state = IDLE_S;
		trig_valid_i = 1'b0;
	end
	
	else begin  // not rst
		trig_valid_i = 1'b0;
		if(fifo_data_valid && pll_locked) begin
			if(state == IDLE_S) begin
				trig_valid_i = 1'b0;
				case (fifo_data)
					8'h2B: begin   // 000T
						trig_sr_i = 4'b0001;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h2D: begin   // 00T0
						trig_sr_i = 4'b0010;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h2E: begin   // 00TT
						trig_sr_i = 4'b0011;
						cmd_valid_i  = 1'b0;
						state = TRIGGER_S;
					end
					8'h33: begin   // 0T00
						trig_sr_i = 4'b0100;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h35: begin   // 0T0T
						trig_sr_i = 4'b0101;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h36: begin   // 0TT0
						trig_sr_i = 4'b0110;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h39: begin   // 0TTT
						trig_sr_i = 4'b0111;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h3A: begin   // T000
						trig_sr_i = 4'b1000;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h3C: begin   // T00T
						trig_sr_i = 4'b1001;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h4B: begin   // T0T0
						trig_sr_i = 4'b1010;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h4D: begin   // T0TT
						trig_sr_i = 4'b1011;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h4E: begin   // TT00
						trig_sr_i = 4'b1100;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h53: begin   // TT0T
						trig_sr_i = 4'b1101;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h55: begin   // TTT0
						trig_sr_i = 4'b1110;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					8'h56: begin   // TTTT
						trig_sr_i = 4'b1111;
						cmd_valid_i = 1'b0;
						state = TRIGGER_S;
					end
					default: begin  // trigger not found, processing as data
						trig_sr_i = 4'b0000;
						cmd_valid_i = 1'b1;
						state = IDLE_S;
					end
				endcase
			end
			else begin
				trig_valid_i = 1'b1;
				if (trig_done) begin
					state = IDLE_S;
					cmd_valid_i = 1'b1;
				end;
				
				if      (fifo_data == 8'h6A) trig_tag = 5'd0;
				else if (fifo_data == 8'h6C) trig_tag = 5'd1;
				else if (fifo_data == 8'h71) trig_tag = 5'd2;
				else if (fifo_data == 8'h72) trig_tag = 5'd3;
				else if (fifo_data == 8'h74) trig_tag = 5'd4;
				else if (fifo_data == 8'h8B) trig_tag = 5'd5;
				else if (fifo_data == 8'h8D) trig_tag = 5'd6;
				else if (fifo_data == 8'h8E) trig_tag = 5'd7;
				else if (fifo_data == 8'h93) trig_tag = 5'd8;
				else if (fifo_data == 8'h95) trig_tag = 5'd9;
				else if (fifo_data == 8'h96) trig_tag = 5'd10;
				else if (fifo_data == 8'h99) trig_tag = 5'd11;
				else if (fifo_data == 8'h9A) trig_tag = 5'd12;
				else if (fifo_data == 8'h9C) trig_tag = 5'd13;
				else if (fifo_data == 8'hA3) trig_tag = 5'd14;
				else if (fifo_data == 8'hA5) trig_tag = 5'd15;
				else if (fifo_data == 8'hA6) trig_tag = 5'd16;
				else if (fifo_data == 8'hA9) trig_tag = 5'd17;
				else if (fifo_data == 8'hAA) trig_tag = 5'd18;
				else if (fifo_data == 8'hAC) trig_tag = 5'd19;
				else if (fifo_data == 8'hB1) trig_tag = 5'd20;
				else if (fifo_data == 8'hB2) trig_tag = 5'd21;
				else if (fifo_data == 8'hB4) trig_tag = 5'd22;
				else if (fifo_data == 8'hC3) trig_tag = 5'd23;
				else if (fifo_data == 8'hC5) trig_tag = 5'd24;
				else if (fifo_data == 8'hC6) trig_tag = 5'd25;
				else if (fifo_data == 8'hC9) trig_tag = 5'd26;
				else if (fifo_data == 8'hCA) trig_tag = 5'd27;
				else if (fifo_data == 8'hCC) trig_tag = 5'd28;
				else if (fifo_data == 8'hD1) trig_tag = 5'd29;
				else if (fifo_data == 8'hD2) trig_tag = 5'd30;
				else if (fifo_data == 8'hD4) trig_tag = 5'd31;
				else trig_tag = 5'dx;
			end
		end
	end
end

always @(posedge clk80 or posedge rst) begin
   if (rst) begin
      trig_sr   <= 4'h0;
      trig_valid <= 1'b0;
   end
   else begin
      trig_sr   <= trig_sr_i;
      trig_valid <= trig_valid_i;
   end
end


//NICO: BCR counter
always @(posedge clk80 or posedge rst or posedge BCR) begin
	if (rst || BCR) begin
		BCID_cnt = 16'h0000;
	end
	else begin
		BCID_cnt = BCID_cnt + 1'b1;
	end
end


trigger_counter count_trigger_i (
    .rst(rst || !pll_locked),
    .clk80(clk80),
    .datain(trig_sr_i),
    .trig_out(trig_out),
    .trig_done(trig_done)
);

assign ECR_o = ECR;
assign BCR_o = BCR;
command_process process_cmd_in_i (
    .rst(rst || !pll_locked),
    .clk80(clk80),
    .data_in_valid(cmd_valid && cmd_valid_i),
    .data_in(fifo_data),
    .chip_id(chip_id),
    .ECR(ECR), .BCR(BCR), .pulse(pulse_o), .cal(cal_o), 
    .wrreg(wrreg_o), .rdreg(rdreg_o), .sync(sync_o),  // connectable if useful
    .data_out_valid(wr_cmd),
    .data_out(processed_cmd),
    .register_address(processed_adx),
    .register_address_valid(wr_adx),
    .CAL_edge(CAL_edge),
    .CAL_aux(CAL_aux),
    .config_reg_out(config_reg) 
);

data_generator dataGenerator_Impl (
    .rst(rst || !pll_locked),
    .clk40(clk40),
    .clk80(clk80),
    .clk160(clk160),
    .triggerClump(trig_sr),
	.trig_tag(trig_tag),
	.BCID_cnt(BCID_cnt),
    .trig_valid(trig_valid),
    .data_out(processed_hit),
    .data_valid(processed_hit_val),
    .n_hits(n_hits_in)
);

assign autoreg_addA[0] = config_reg[101];
assign autoreg_addB[0] = config_reg[102];
assign autoreg_addA[1] = config_reg[103];
assign autoreg_addB[1] = config_reg[104];
assign autoreg_addA[2] = config_reg[105];
assign autoreg_addB[2] = config_reg[106];
assign autoreg_addA[3] = config_reg[107];
assign autoreg_addB[3] = config_reg[108];

assign autoreg_dataA[0] = {{1'b0}, {autoreg_addA[0]}, {config_reg[autoreg_addA[0]]}};
assign autoreg_dataB[0] = {{1'b0}, {autoreg_addB[0]}, {config_reg[autoreg_addB[0]]}};
assign autoreg_dataA[1] = {{1'b0}, {autoreg_addA[1]}, {config_reg[autoreg_addA[1]]}};
assign autoreg_dataB[1] = {{1'b0}, {autoreg_addB[1]}, {config_reg[autoreg_addB[1]]}};
assign autoreg_dataA[2] = {{1'b0}, {autoreg_addA[2]}, {config_reg[autoreg_addA[2]]}};
assign autoreg_dataB[2] = {{1'b0}, {autoreg_addB[2]}, {config_reg[autoreg_addB[2]]}};
assign autoreg_dataA[3] = {{1'b0}, {autoreg_addA[3]}, {config_reg[autoreg_addA[3]]}};
assign autoreg_dataB[3] = {{1'b0}, {autoreg_addB[3]}, {config_reg[autoreg_addB[3]]}};

cmd_out RD53_cmd_out_controller(
	.rst(rst),
	.clk40(clk40),
	.clk80(clk80),
	.clk160(clk160),
	.hit_in(processed_hit),
	.hit_valid(processed_hit_val),
	.reg_data_wr(wr_cmd && !cmd_full),
	.reg_data(processed_cmd),
	.reg_addr(processed_adx),
	.cmd_full(cmd_full),
	.autoreg_data_A0(autoreg_dataA[0]),
	.autoreg_data_A1(autoreg_dataA[1]),
	.autoreg_data_A2(autoreg_dataA[2]),
	.autoreg_data_A3(autoreg_dataA[3]),
	.autoreg_data_B0(autoreg_dataB[0]),
	.autoreg_data_B1(autoreg_dataB[1]),
	.autoreg_data_B2(autoreg_dataB[2]),
	.autoreg_data_B3(autoreg_dataB[3]),
	.aurora_head_out(aurora_head_out),
	.aurora_data_out0(aurora_data_out[3]),																			   
	.aurora_data_out1(aurora_data_out[2]),
	.aurora_data_out2(aurora_data_out[1]),
	.aurora_data_out3(aurora_data_out[0]),
	.n_data_frames(48)
);
endmodule
