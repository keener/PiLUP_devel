create_clock -period 8.333 -name SMA_MGT_REFCLK_P -waveform {0.000 4.167} [get_ports SMA_MGT_REFCLK_P]
create_generated_clock -name REC_CLOCK_C_P -source [get_pins clk_manager/ODDR_inst/C] -divide_by 1 -invert [get_ports REC_CLOCK_C_P]
create_generated_clock -name USER_SMA_CLOCK_P -source [get_pins clk_manager/ODDR2_inst/C] -divide_by 1 -invert [get_ports USER_SMA_CLOCK_P]
create_clock -period 25.000 -name VIRTUAL_clk40_out_clk_wiz_200 -waveform {0.000 12.500}
set_output_delay -clock [get_clocks SYSCLK_P] -min -add_delay 0.000 [get_ports SFP_TX_DISABLE]
set_output_delay -clock [get_clocks SYSCLK_P] -max -add_delay 2.000 [get_ports SFP_TX_DISABLE]



set_clock_groups -name CLOCK_GROUPS -asynchronous \
    -group [get_clocks [list \
        VIRTUAL_clk40_out_clk_wiz_200 \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_sysclk/inst/mmcm_adv_inst/CLKOUT1]] \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_sysclk/inst/mmcm_adv_inst/CLKOUT0]]]] \
    -group [get_clocks [list \
        RD53_emulator/RD53_clock/inst/clk_in1 \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_recclk/inst/mmcm_adv_inst/CLKOUT0]] \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_recclk/inst/mmcm_adv_inst/CLKOUT4]] \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_recclk/inst/mmcm_adv_inst/CLKOUT1]] \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_recclk/inst/mmcm_adv_inst/CLKOUT2]] \
        [get_clocks -of_objects [get_pins clk_manager/clock_wizard_recclk/inst/mmcm_adv_inst/CLKOUT3]]]] \
    -group [get_clocks [list \
        [get_clocks -of_objects [get_pins RD53_emulator/RD53_clock/inst/mmcm_adv_inst/CLKOUT0]] \
        [get_clocks -of_objects [get_pins RD53_emulator/RD53_clock/inst/mmcm_adv_inst/CLKOUT4]] \
        [get_clocks -of_objects [get_pins RD53_emulator/RD53_clock/inst/mmcm_adv_inst/CLKOUT1]] \
        [get_clocks -of_objects [get_pins RD53_emulator/RD53_clock/inst/mmcm_adv_inst/CLKOUT2]] \
        [get_clocks -of_objects [get_pins RD53_emulator/RD53_clock/inst/mmcm_adv_inst/CLKOUT3]]]] \
    -group [get_clocks {GBT_FullMode_connection/gtx_transceiver/transceiver_inst[0].GBT_FM_transceiver_inst/U0/GBT_FM_transceiver_gtx_i/gt0_GBT_FM_transceiver_gtx_i/gtxe2_i/RXOUTCLK}] \
    -group [get_clocks {GBT_FullMode_connection/gtx_transceiver/transceiver_inst[0].GBT_FM_transceiver_inst/U0/GBT_FM_transceiver_gtx_i/gt0_GBT_FM_transceiver_gtx_i/gtxe2_i/TXOUTCLK}]


set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets sysclk40]
