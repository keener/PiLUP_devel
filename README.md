# PiLUP Development Repository      

## KINTEX FIRMWARE
Currently supported firmwares:

1. **template** ---> starting point for firmware generation.
   Contains reset_from_software registers + DATE, TIME, and HASH registers

2. **Protocol_Converter** ----> RD53A emulator + interface with Felix

3. **Data_Generator** ---> random data generator firmware

### Create Project
````bash
cd Firmware/Kintex
vivado -mode batch -notrace -source generate_project.tcl
````
Select project when asked. The new Project will be generated in the directory
*Firmware/Kintex/PiLUP\_Kintex\_$PROJ_NAME*

### Build Project
````bash
cd Firmware/Kintex
vivado -mode batch -notrace -source generate_bitstream.tcl
````
Select project when asked. The built files will be generated in the directory
*Firmware/Kintex/PiLUP\_Kintex\_$PROJ_NAME/run*
(bitfile and ltx file in *impl_1* directory).

### Create new Project
````bash
cd Firmware/Kintex
cp -r template/ $NEW_PROJECT_NAME
mv $NEW_PROJECT_NAME/src/PiLUP_Kintex_template.vhd $NEW_PROJECT_NAME/src/$NEW_PROJECT_NAME_TOP.vhd 
vivado -mode batch -notrace -source generate_project.tcl
````
Select your project directory name when asked by the generate_project tcl.
Open your project with Vivado and start editing.

### Add new register
- in file ***Firmware/Kintex/${PROJ_NAME}/src/control_registers/register_pkg.vhd***:

1. Update numer of **control** and **status** registers
````vhdl
constant N_CTRL_REGS: integer	:= #{NUMBER_OF_CTRL_REGS};
constant N_STATUS_REGS: integer	:= #{NUMBER_OF_STATUS_REGS}; 
```` 

2. Define new register **local address** and **bit range** (multiple registers per address is allowed)
````vhdl
constant #{NEW_REG}_REG: natural := #{REG_ADDRESS};
constant #{NEW_REG}_RANGE: natural := #{BIT_RANGE}; -- only 1 bit
subtype #{NEW_REG}_RANGE is natural range #{BITS_RANGE_HIGH} downto #{BITS_RANGE_LOW}; -- multiple bits
````

- in file ***project top file***:

1. Connect the **control** and **status** registers

````vhdl
-- register connection
--ctrl registers
#{NEW_CTRL_REG}	<= ctrl#{NEW_CTRL_REG}_REG)(#{NEW_CTRL_REG}_RANGE);
-- status registers
status(#{NEW_STS_REG}_REG)(#{NEW_STS_REG}_RANGE)<= #{NEW_STS_REG};
````

#### Example: 
**Control Registers**



| Address \ bits  | 31 - 2 | 1        | 0        |
| --------------- | ------ | -------- | -------- |
| 0               |  0     | 0        | reset    |
| 1               | 0      | enable_B | enable_A |


**Status Registers**

| Address \ bits  | 31 - 16    | 15 - 8  | 7 - 0   |
| --------------- | ---------- | ------- | ------- |
| 0               |  0         | count_1 | count_2 |
| 1               | cnt_16bits | 0       | 0       |

**register_pkg.vhd:**
````vhdl
.....
-- CONTROL registers
constant N_CTRL_REGS							: integer	:= 2;

constant reset_REG								: natural := 0;
constant reset_RANGE							: natural := 0;

constant enable_A_REG							: natural := 1;
constant enable_A_RANGE						    : natural := 0;

constant enable_B_REG							: natural := 1;
constant enable_B_RANGE						    : natural := 1;


-- STATUS registers
constant N_STATUS_REGS							: integer	:= 2;

constant count_1_REG							: natural := 0;
subtype count_1_RANGE								is natural range 15 downto 8;
		
constant count_2_REG							: natural := 0;
subtype count_2_RANGE								is natural range 7 downto 0;

constant cnt_16bits_REG							: natural := 1;
subtype cnt_16bits_RANGE							is natural range 31 downto 16;

...
````

**{TOP}.vhd:**
````vhdl
...
signal reset, enable_A, enable_B				: std_logic;
signal count_1, count_2							: std_logic_vector(7 downto 0);
signal cnt_16bits								: std_logic_vector(15 downto 0);
...
reset							<= ctrl(reset_REG)(reset_RANGE);
enable_A							<= ctrl(enable_A_REG)(enable_A_RANGE);
enable_B						<= ctrl(enable_B_REG)(enable_B_RANGE);
	
-- status registers
status(count_1_REG)(count_1_RANGE)		        <= count_1;
status(count_2_REG)(count_2_RANGE)		        <= count_2;
status(cnt_16bits_REG)(cnt_16bits_RANGE)		<= cnt_16bits;
...
````
